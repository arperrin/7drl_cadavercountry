///scr_post_message(message)
var line = argument0;
var tempLine = "";
var tempList = ds_list_create();
var word = "";
var max_length = obj_status_window.max_string_width;

// split the line if needed

// for each character in the line...
for (i = 1; i <= string_length(line); i++)
{
  // add the character to a word
  word += string_char_at(line, i);

  // if the word is complete
  if (string_char_at(line, i) == ' ' || i == string_length(line))
  {
    // if I add this word, will the line run over?
    if (string_width(tempLine + word) > max_length)
    {
      // if so, add the line to the list, and start a new line
      ds_list_add(tempList, tempLine);
      tempLine = " " + word;
    }
    // otherwise, add the word to the line.
    else 
    {
      tempLine += word;
    }
    
    // Move on to next word
    word = "";
  }
}

// add whatever's left
ds_list_add(tempList, tempLine);

// add a new messsage
for (i = 0; i < ds_list_size(tempList); i++)
{
  ds_list_add(obj_status_window.lines, ds_list_find_value(tempList, i));
}

// cleanup
ds_list_destroy(tempList);

// prune old messages if needed
with(obj_status_window)
{
  while (line_height * ds_list_size(lines) > max_lines_height)
  {
    ds_list_delete(lines, 0);
  }
}
