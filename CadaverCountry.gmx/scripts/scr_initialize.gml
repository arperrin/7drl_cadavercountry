///scr_initialize();
randomize();

// Globals
globalvar DEBUG; DEBUG = false;
globalvar STEP_SIZE; STEP_SIZE = 32;
globalvar TOP_BORDER; TOP_BORDER = 0;
globalvar BOTTOM_BORDER; BOTTOM_BORDER = room_height;
globalvar RIGHT_BORDER; RIGHT_BORDER = room_width;
globalvar LEFT_BORDER; LEFT_BORDER = 0;
globalvar GAME_STARTED; GAME_STARTED = false;
globalvar GAME_OVER; GAME_OVER = false;

// Loot table
//instance_create(0,0,obj_loot_table);

// Party window
//instance_create(0,0,obj_party_window);

// Hud
//instance_create(320,608,obj_hud);

// Create status window
//instance_create(0,0,obj_status_window);


if (DEBUG)
{
  instance_create(0,0,obj_debug_helper);
}
