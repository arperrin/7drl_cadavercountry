///scr_gen_loot(type, loot_list, max_number_of_items)
var type = argument0;
var loot = argument1;
var max_count = argument2;
var sum_item_chance = obj_loot_table.sum_item_chance[type];

for(i = 0; i < max_count; i++)
{
  // generate item number
  newItem = irandom(sum_item_chance);
  show_debug_message("Generated item code: " + string(newItem));
  
  // translate to item string
  for(j = 0; j <= array_height_2d(obj_loot_table.table); j++)
  {
    newItem -= obj_loot_table.table[j, type];
    if (newItem <= 0)
    {
      newItem = obj_loot_table.table[j, obj_loot_table.name];
      break;
    }
  }
  
  show_debug_message("Translated item code to item: " + string(newItem));
  
  // add item to list
  if (newItem != "null") ds_list_add(loot, newItem);
}
