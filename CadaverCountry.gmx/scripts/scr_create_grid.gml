///scr_create_grid()
var newGrid = instance_create(0,0,obj_grid);
with (newGrid)
{
  startx = 0;
  starty = 0;
  width = RIGHT_BORDER - LEFT_BORDER;
  height = BOTTOM_BORDER - TOP_BORDER;
  cell_size = STEP_SIZE;
  draw_border = false;
  line_color = c_gray;
}

