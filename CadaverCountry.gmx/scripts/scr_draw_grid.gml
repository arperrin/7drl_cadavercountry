///scr_draw_grid(startx, starty, width, height, cell_size, draw_border, line_color)
var startx = argument0;
var starty = argument1;
var width = argument2;
var height = argument3;
var cell_size = argument4;
var draw_border = argument5;
var line_color = argument6;

var done = false;

var columns = width / cell_size;
var rows = height / cell_size;

var cCol = 1;
var cRow = 1;

draw_set_color(argument6);

// draw grid from x to y, width to height
if (draw_border)
{
  draw_rectangle(startx, starty, startx + width, starty + height, true);
}

while(!done)
{
  x1 = startx + cCol * cell_size;
  x2 = x1;
  y1 = starty;
  y2 = y1 + height;
  draw_line(x1, y1, x2, y2);
  
  cCol++;
  
  if (cCol > columns) 
    done = true;
}

done = false;
while(!done)
{
  x1 = startx;
  x2 = x1 + width;
  y1 = starty + cRow * cell_size;
  y2 = y1;
  draw_line(x1, y1, x2, y2);
  
  cRow++;
  
  if(cRow > rows)
    done = true;
}

draw_set_color(c_black);