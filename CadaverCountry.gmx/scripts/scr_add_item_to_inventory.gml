///scr_add_item_to_inventory(item)
var item = argument0;

for (i = 0; i < array_height_2d(obj_player.inventory); i++)
{
  if (string(obj_player.inventory[i,0]) == string(item))
  {
    obj_player.inventory[i,1]++;
    return true;
  }
}

obj_player.inventory[inventory_count,0] = item;
obj_player.inventory[inventory_count++,1] = 1;

return true;