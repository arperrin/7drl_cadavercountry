///scr_draw_info_window(x, y, window_margin, lines, should_adjust)
var xx = argument0;
var yy = argument1;
var width = 0;
var height = 0;
var lines = argument3;
var text_padding = 6;
var line_height = string_height("Test String");
var window_margin = argument2; // Get it away from mouse cursor
var should_adjust = argument4;

// Calculate width, height of window
var max_width = 0;
for(i = 0; i < ds_list_size(lines); i++)
{
  var current_line = ds_list_find_value(lines, i);
  if (max_width < string_width(current_line))
  {
    max_width = string_width(current_line);
  }
}

width = max_width + text_padding * 2;
height = ds_list_size(lines) * line_height + text_padding * 2;

// Adjust xx and yy if needed
if (should_adjust && xx + width > room_width - 10)
{
  xx = xx - width;
  xx -= window_margin;
}
else
{
  xx += window_margin;
}

if (should_adjust && yy + height > room_height - 10)
{
  yy = yy - height;
  yy -= window_margin;
}
else
{
 yy += window_margin
}

// Draw background
draw_set_color(c_black);
draw_rectangle(xx, yy, xx + width, yy + height, false);
  
// Draw border
draw_set_color(c_white);
draw_rectangle(xx, yy, xx + width, yy + height, true);
  
// Draw information
for(i = 0; i < ds_list_size(lines); i++)
{
  draw_text(xx + text_padding, yy + text_padding + line_height * i, ds_list_find_value(lines, i));
}

// Reset drawing color;  
draw_set_color(c_black);
