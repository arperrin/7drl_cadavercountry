///scr_path_find(starting_x, starting_y, target_x, target_y)
// returns: array of steps that make up a path.

startx = argument0;
starty = argument1;
targetx = argument2;
targety = argument3;

var steps;

// calculate 4 points
points[0,0] = startx; // north
points[0,1] = starty - STEP_SIZE;
points[1,0] = startx; // south
points[1,1] = starty + STEP_SIZE;
points[2,0] = startx - STEP_SIZE; // west
points[2,1] = starty;
points[3,0] = startx + STEP_SIZE; // east
points[3,1] = starty;

var shortestx = startx;
var shortesty = starty;
var shortestd = room_width;

for (var i = 0; i < array_height_2d(points); i++)
{  
  // eliminate points that are occupied
  if (place_meeting(points[i,0], points[i,1], obj_house) 
  || place_meeting(points[i,0], points[i,1], obj_zombie))
  {
  }
  // eliminate points that are off the map
  else if (startx - STEP_SIZE < LEFT_BORDER || startx + STEP_SIZE >= RIGHT_BORDER
  || starty - STEP_SIZE < TOP_BORDER || starty + STEP_SIZE >= BOTTOM_BORDER)
  {
  }
  // check distance
  else if (point_distance(points[i,0], points[i,1], targetx, targety) < shortestd)
  {
    shortestx = points[i,0];
    shortesty = points[i,1];
    shortestd = point_distance(points[i,0], points[i,1], targetx, targety);
  }
}

steps[0] = shortestx - startx;
steps[1] = shortesty - starty;
return steps;

