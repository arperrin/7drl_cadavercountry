///scr_use_inventory_item(pos);
var inventory = argument0;
var pos = argument1;
var inventory_length = array_height_2d(inventory);
var used_item = false;

if (pos > inventory_length - 1) return false; // ignore keypress, nothing there

var item = string(inventory[pos,0]);
if (item == "0") return false; // item is blank (but passed the length check earlier)

// Would be great place to have a custom event? (i.e. Medkit.Use());
switch(item)
{
  case("Bandages"): 
    if (obj_player.hp < obj_player.max_hp)
    {
      scr_post_message("Used bandages.");
      obj_player.hp += 20;
      if (obj_player.hp > obj_player.max_hp) obj_player.hp = obj_player.max_hp;
      used_item = true;
    }
    else
      scr_post_message("I shouldn't waste these bandages.");
  break;
  
  case("Bottled Water"):
    if (obj_player.thirst > 10)
    {
      scr_post_message("Used bottled water");
      obj_player.thirst -= 30;
      if (obj_player.thirst < 0) obj_player.thirst = 0;
      used_item = true;
    }
    else
      scr_post_message("I'm really not that thirsty!");
  break;
  
  case("Beef Jerky"):
    if (obj_player.hunger > 0)
    {
      scr_post_message("Used beef jerky.");
      obj_player.hunger -= 10;
      if (obj_player.hunger < 0) obj_player.hunger =0;
      used_item = true;      
    }
    else
      scr_post_message("I'm really not that hungry!");
  break;
  
  case("Baseball Bat"):
    if (obj_player.weapon != "none")
    {
      scr_add_item_to_inventory(obj_player.weapon);
    }
    
    obj_player.weapon = "Baseball Bat";
    obj_player.weapon_durability = 10;
    obj_player.weapon_damage = 2;  
    scr_post_message("Equipped baseball bat.");  
  break;
  
  case("Kitchen Knife"):
    if (obj_player.weapon != "none")
    {
      scr_add_item_to_inventory(obj_player.weapon);
    }
    
    obj_player.weapon = "Kitchen Knife";
    obj_player.weapon_durability = 10;
    obj_player.weapon_damage = 3;    
    scr_post_message("Equipped kitchen knife");
  break;

  default:
  scr_post_message("I don't know how to use that!");
  break;
}

if (used_item)
{
  inventory[@pos,1]--;

  // clean inventory
  var temp;temp[0,0]=0;temp[0,1]=0;  
  var j = 0;
  for (var i = 0; i < array_height_2d(inventory); i++)
  {
    if (inventory[i,1] > 0)
    {
      temp[j,0] = inventory[i,0];
      temp[j,1] = inventory[i,1];
      j++;
    }
  }  
  
  // TODO: I'm directly referencing the player's inventory because assigning
  // temp to inventory (local variable) doesn't ever change the player's inventory.
  // This would be fixed by having an inventory object and letting it manage itself?
  obj_player.inventory = temp;
  
  if (string(temp[0,0]) == "0")
    obj_player.inventory_count = 0;
  else
    obj_player.inventory_count = array_height_2d(temp);
  
  return true;
}
