///scr_spawn_enemies(building, number_to_spawn)
var building = argument0;
var number_to_spawn = argument1;

// figure out building dimensions, and possible spawn points.
var spawn_points;
var j = 0; // spawn_point_count

// spawn points across top and bottom of building
for (var i = 0; i < building.sprite_width/STEP_SIZE; i++)
{
  // across the top, don't spawn on player
  if (!(obj_player.y == building.y - STEP_SIZE && obj_player.x == building.x + STEP_SIZE*i))
  {
    spawn_points[j,0] = building.x + STEP_SIZE*i;
    spawn_points[j,1] = building.y - STEP_SIZE;
    j++;
  }
  
  // across the bottom
  if (!(obj_player.y == building.y + building.sprite_height && obj_player.x == building.x+STEP_SIZE*i))
  {
    spawn_points[j,0] = building.x + STEP_SIZE*i;
    spawn_points[j,1] = building.y + building.sprite_height;
    j++;
  }
}

// spawn points across left and right of building
var temp = array_height_2d(spawn_points); // don't want to re-evaluate the height every trip through the loop (inf loop)
for (var i = temp; i < temp + building.sprite_height/STEP_SIZE; i++)
{
  // down left side
  if (!(obj_player.y == building.y + STEP_SIZE*(i - temp) && obj_player.x == building.x - STEP_SIZE))
  {
    spawn_points[j,0] = building.x - STEP_SIZE;
    spawn_points[j,1] = building.y + STEP_SIZE*(i - temp);
    j++;
  }
  
  // down right side
  if (!(obj_player.y == building.y + STEP_SIZE*(i - temp) && obj_player.x == building.x + building.sprite_width))
  {
    spawn_points[j,0] = building.x + building.sprite_width;
    spawn_points[j,1] = building.y + STEP_SIZE*(i - temp);
    j++;
  }
}

// debug spawn enemy at each spot
for(var i = 0; i < array_height_2d(spawn_points); i++)
{ 
//  instance_create(spawn_points[i,0], spawn_points[i,1], obj_zombie);
}

// randomly pick a few spawn points.
for (var i = 0; i < number_to_spawn; i++)
{
  var point = irandom(array_height_2d(spawn_points)-1);
  instance_create(spawn_points[point,0], spawn_points[point,1], obj_zombie);
  
  // remove row from array
  var tempArray;
  for(var j = 0, k = 0; k < array_height_2d(spawn_points); k++)
  {
    if(k != point) 
    {
      tempArray[j,0] = spawn_points[k,0];
      tempArray[j,1] = spawn_points[k,1];
      j++;
    }
  }  
  spawn_points = tempArray;
}
