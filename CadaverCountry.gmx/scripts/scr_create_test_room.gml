///scr_create_test_room(playerx, playery)
// Create Player
instance_create(argument0, argument1, obj_player);

// Debug object
instance_create(0,0,obj_debug_helper);

// Create Buildings
instance_create(128,128,obj_house);
instance_create(352,448,obj_house);
instance_create(736,256,obj_house);
instance_create(960,704,obj_house);
instance_create(960,224,obj_house);
